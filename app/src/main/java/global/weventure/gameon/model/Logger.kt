package global.weventure.gameon.model

import android.util.Log
import com.example.js.BuildConfig

object Logger {
    private const val TAG = "Payment"

    fun d(mes: String) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, mes)
        }
    }

    fun d(tag: String, mes: String) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, mes)
        }
    }

    fun w(mes: String) {
        if (BuildConfig.DEBUG) {
            Log.w(TAG, mes)
        }
    }

    fun w(tag: String, mes: String) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, mes)
        }
    }

    fun e(mes: String) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, mes)
        }
    }

    fun e(tag: String, mes: String) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, mes)
        }
    }
}

