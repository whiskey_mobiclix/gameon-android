package global.weventure.gameon.model

import com.android.billingclient.api.SkuDetails
import global.weventure.payment.model.AppBillingClient

class OneTimeModel(override val skuDetails: SkuDetails) : AppBillingClient.ProductModel()