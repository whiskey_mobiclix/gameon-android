package global.weventure.gameon

import android.content.Context
import android.webkit.JavascriptInterface

class WebAppInterface(
    private val mContext: Context,
    private val mOnPurchase: (sku: String, skuType: String) -> Unit
) {
    /**
     * [sku] : the product id was set on the Google Play Store
     * Ex: global.weventure.product.gameon.10_coin_package
     *
     * [skuType] : it is inapp(one time product) or subs(subscription product)
     * @see: [com.android.billingclient.api.BillingClient.SkuType]
     */
    @JavascriptInterface
    fun purchaseViaGoogle(sku: String, skuType: String) {
        mOnPurchase(sku, skuType)
    }
}