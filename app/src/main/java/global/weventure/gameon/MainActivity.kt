package global.weventure.gameon

import android.os.Build
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import com.example.js.R
import global.weventure.gameon.model.Logger
import global.weventure.gameon.model.OneTimeModel
import global.weventure.payment.view.PaymentActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : PaymentActivity() {
    private val mPendingPurchaseList: ArrayList<Purchase> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(wvWebView, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        wvWebView?.let {
            it.webViewClient = WebViewClient()
            it.settings.javaScriptEnabled = true
            it.settings.useWideViewPort = true
            it.settings.loadWithOverviewMode = true
            it.settings.builtInZoomControls = true
            it.settings.displayZoomControls = false
            it.webChromeClient = WebChromeClient() // Enable to show Alert in Javascript
            it.addJavascriptInterface(
                WebAppInterface(this, ::onPurchaseViaGoogle),
                "Android"
            )
//            it.loadUrl("https://game-on.live")
            it.loadUrl("file:///android_asset/test.html")
        }
    }

    override fun onBackPressed() {
        wvWebView.goBack()
    }

    override fun getContentView(): Int = R.layout.activity_main

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // The state of the BillingClient
    ////////////////////////////////////////////////////////////////////////////////////////////////

    override fun onBillingClientStateChange(isConnected: Boolean) {
        if (isConnected) {
            // We only get the pending purchase when BillingClient is connected
            loadPendingVerifyProduct { purchases ->
                if (purchases.isNotEmpty()) {
                    mPendingPurchaseList.addAll(purchases)
                    Logger.d("MainActivity -> loadPendingVerifyProduct -> size: ${purchases.size}")
                    // TODO: need discuss with Whiskey -> how/when to send these data to Javascript
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Start to purchase via Google, using native code
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * [sku] : the product id was set on the Google Play Store
     * Ex: global.weventure.product.gameon.10_coin_package
     *
     * [skuType] : it is "inapp"(one time product) or "subs"(subscription product)
     * @see: [com.android.billingclient.api.BillingClient.SkuType]
     */
    private fun onPurchaseViaGoogle(sku: String, skuType: String) {
        Logger.d("MainActivity -> onPurchaseViaGoogle -> connected $isBillingClientConnected")
        if (isBillingClientConnected) {
            findSkuDetail(sku, skuType) { result ->
                result?.run {
                    buyProductOnGoogle(
                        OneTimeModel(this),
                        // Purchase success
                        { purchases ->
                            Logger.d("MainActivity -> buy item success")

                            purchases.forEach { p ->
                                if (p.sku == sku) {
                                    onPurchaseViaGoogleSuccess(
                                        p.purchaseToken,
                                        p.originalJson,
                                        p.signature
                                    )
                                    return@buyProductOnGoogle
                                }
                            }
                        },
                        // Purchase failed
                        { errorCode ->
                            Logger.d("MainActivity -> buy item failed")
                            onPurchaseViaGoogleFailed(errorCode)
                        }
                    )
                }
            }
        }
    }

    /**
     * [purchaseToken]: Using it to verify with our server
     * [purchaseData]: this is a Json string that return from Google with the below format
     * {
     *      "orderId": "GPA.3338-3394-9301-68224",
     *      "packageName": "global.weventure.product.pulse",
     *      "productId": "global.weventure.product.pulse.tier3.150_coin_package",
     *      "purchaseTime": 1584441998808,
     *      "purchaseState": 0,
     *      "purchaseToken": "momnkfjbmijcinomnnopnbfo.AO-J1OzJfY2HFIkAuFPhEzuMELcQ9tRTQxI3jZJJHFKeUnCqEf4ZaLHSqROeCwoiCbYalkk7OAc2UOmWLaJBrB7Pu0O7YZiT8n_YXR8M9Xg6zHDQjeEYS4MVZLOFMJFFy6Vut96WYp8z9PDX6Awm3aQfVhjfOowTLHqb4e-WG7eii1QmXM8OyLblz79pRfeXgv-6FDyXKzz2",
     *      "acknowledged": false
     * }
     *
     * [signature]: a signature that return from Google
     */
    private fun onPurchaseViaGoogleSuccess(
        purchaseToken: String,
        purchaseData: String,
        signature: String
    ) {
        wvWebView?.run {
            callJavaScript(
                this,
                "onPurchaseViaGoogleSuccess",
                purchaseToken,
                purchaseData,
                signature
            )
        }
    }

    /**
     * [errorCode]: this is one of these values [BillingClient.BillingResponseCode]
     */
    private fun onPurchaseViaGoogleFailed(errorCode: Int) {
        wvWebView?.run {
            // Parse error code to string
            val errorCodeStr = parseErrorCodeToString(errorCode)
            callJavaScript(this, "onPurchaseViaGoogleFailed", errorCodeStr)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Start to return response to Javascript code
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * [view]: this is [WebView]
     * [methodName]: method name is in the Javascript code
     * [params]: array of the parameters which will be put as arguments of the method
     */
    private fun callJavaScript(view: WebView, methodName: String, vararg params: Any) {
        val stringBuilder = StringBuilder()
        stringBuilder.append("try{")
        stringBuilder.append(methodName)
        stringBuilder.append("(")
        var separator = ""
        for (param in params) {
            stringBuilder.append(separator)
            separator = ","
            if (param is String) {
                stringBuilder.append("'")
            }
            stringBuilder.append(param.toString().replace("'", "\\'"))
            if (param is String) {
                stringBuilder.append("'")
            }
        }
        stringBuilder.append(");}catch(error){console.error(error.message);}")
        val call = stringBuilder.toString()

        Logger.d("MainActivity -> callJavaScript: call = $call")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            view.evaluateJavascript(call, null)
        } else {
            view.loadUrl("javascript:$call")
        }
    }
}
