package global.weventure.payment.model

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable

object BroadcastUtil {
    const val RECEIVER_BILLING_CLIENT_STATE_CHANGE = "RECEIVER_BILLING_CLIENT_STATE_CHANGE"
    const val INTENT_BILLING_CLIENT_CONNECTED = "INTENT_BILLING_CLIENT_CONNECTED"

    fun sendBroadcast(
        context: Context,
        action: String,
        data: Map<String, Any>? = null,
        mapArrayParcelable: Map<String, Array<Parcelable>>? = null,
        mapArrayCharSequence: Map<String, Array<CharSequence>>? = null,
        mapArrayString: Map<String, Array<String>>? = null
    ) {
        context.sendBroadcast(
            Intent(formatReceiverAction(context, action)).apply {
                data?.let {
                    for ((k, v) in it) {
                        when (v) {
                            is Bundle -> putExtra(k, v)
                            is Parcelable -> putExtra(k, v)
                            is Serializable -> putExtra(k, v)
                            is Boolean -> putExtra(k, v)
                            is BooleanArray -> putExtra(k, v)
                            is Byte -> putExtra(k, v)
                            is ByteArray -> putExtra(k, v)
                            is Char -> putExtra(k, v)
                            is CharArray -> putExtra(k, v)
                            is CharSequence -> putExtra(k, v)
                            is Double -> putExtra(k, v)
                            is DoubleArray -> putExtra(k, v)
                            is Float -> putExtra(k, v)
                            is FloatArray -> putExtra(k, v)
                            is Int -> putExtra(k, v)
                            is IntArray -> putExtra(k, v)
                            is Long -> putExtra(k, v)
                            is LongArray -> putExtra(k, v)
                            is Short -> putExtra(k, v)
                            is ShortArray -> putExtra(k, v)
                            is String -> putExtra(k, v)
                            else -> throw Exception("Unsupported type $v")
                        }
                    }
                }

                mapArrayParcelable?.let {
                    for ((k, v) in it) {
                        putExtra(k, v)
                    }
                }

                mapArrayCharSequence?.let {
                    for ((k, v) in it) {
                        putExtra(k, v)
                    }
                }

                mapArrayString?.let {
                    for ((k, v) in it) {
                        putExtra(k, v)
                    }
                }
            }
        )
    }

    fun formatReceiverAction(context: Context, action: String): String {
        return String.format("%s.%s", context.packageName, action)
    }
}