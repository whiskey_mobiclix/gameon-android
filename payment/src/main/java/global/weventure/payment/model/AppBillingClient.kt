package global.weventure.payment.model

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import com.android.billingclient.api.*
import global.weventure.payment.model.BroadcastUtil.INTENT_BILLING_CLIENT_CONNECTED
import global.weventure.payment.model.BroadcastUtil.RECEIVER_BILLING_CLIENT_STATE_CHANGE

class AppBillingClient private constructor() {
    private lateinit var mBillingClient: BillingClient
    private lateinit var mContext: Context
    private var mBillingServiceStateListener: OnBillingServiceStateListener? = null

    private var mBillingSetupResponseCode: Int =
        BillingClient.BillingResponseCode.SERVICE_DISCONNECTED

    private var mOnPurchaseSuccess: ((purchase: List<Purchase>) -> Unit)? = null
    private var mOnPurchaseError: ((errorCode: Int) -> Unit)? = null

    var isConnectedToBillingService = false
        private set

    private constructor(context: Context) : this() {
        mContext = context

        mBillingClient = BillingClient
            .newBuilder(context)
            .setListener(mPurchasesUpdatedListener)
            .enablePendingPurchases()
            .build()
        mBillingClient.startConnection(mBillingClientStateListener)
    }

    private val mPurchasesUpdatedListener = PurchasesUpdatedListener { billingResult, purchases ->
        val responseCodeStr = getResponseCodeAsString(billingResult.responseCode)
        Logger.d("AppBillingClient -> PurchasesUpdatedListener -> responseCode: $responseCodeStr")

        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            mOnPurchaseSuccess?.invoke(purchases)
        } else {
            mOnPurchaseError?.invoke(billingResult.responseCode)
        }
    }

    private val mBillingClientStateListener = object : BillingClientStateListener {
        override fun onBillingSetupFinished(billingResult: BillingResult) {
            val responseCodeStr = getResponseCodeAsString(billingResult.responseCode)
            isConnectedToBillingService =
                billingResult.responseCode == BillingClient.BillingResponseCode.OK
            mBillingSetupResponseCode = billingResult.responseCode

            sendBroadcastBillingClientStateChange(true)
            mBillingServiceStateListener?.onBillingServiceConnected()

            Logger.d("AppBillingClient -> onBillingSetupFinished -> responseCode: $responseCodeStr")
        }

        override fun onBillingServiceDisconnected() {
            isConnectedToBillingService = false
            mBillingSetupResponseCode = BillingClient.BillingResponseCode.SERVICE_DISCONNECTED

            sendBroadcastBillingClientStateChange(false)
            mBillingServiceStateListener?.onBillingServiceDisconnected()

            Logger.d("AppBillingClient -> onBillingServiceDisconnected")
        }
    }

    private fun sendBroadcastBillingClientStateChange(isConnected: Boolean) {
        BroadcastUtil.sendBroadcast(
            mContext,
            RECEIVER_BILLING_CLIENT_STATE_CHANGE,
            HashMap<String, Boolean>().apply {
                put(INTENT_BILLING_CLIENT_CONNECTED, isConnected)
            }
        )
    }

    fun <T : ProductModel> buyProduct(
        activity: Activity,
        product: T,
        onPurchaseSuccess: (purchase: List<Purchase>) -> Unit,
        onPurchaseError: (errorCode: Int) -> Unit
    ) {
        if (!isConnectedToBillingService) {
            onPurchaseError(BillingClient.BillingResponseCode.SERVICE_DISCONNECTED)
            return
        }

        // Keep to use late
        mOnPurchaseSuccess = onPurchaseSuccess
        mOnPurchaseError = onPurchaseError

        // Ask Google Play for the purchasing
        val flowParams = BillingFlowParams.newBuilder()
            .setSkuDetails(product.skuDetails)
            .build()
        mBillingClient.launchBillingFlow(activity, flowParams)
    }

    /**
     * M360-279: Android - Coding coin screen
     */
    fun querySkuDetailsAsync(
        skuList: ArrayList<String>,
        @BillingClient.SkuType skuType: String,
        listener: SkuDetailsResponseListener
    ) {
        if (!isConnectedToBillingService) {
            listener.onSkuDetailsResponse(
                BillingResult.newBuilder().setResponseCode(BillingClient.BillingResponseCode.SERVICE_DISCONNECTED).build(),
                ArrayList()
            )
            return
        }

        val params = SkuDetailsParams.newBuilder()
            .setSkusList(skuList)
            .setType(skuType)
        mBillingClient.querySkuDetailsAsync(params.build(), listener)
    }

    fun consumeAsync(
        purchaseToken: String,
        listener: ConsumeResponseListener,
        payload: DeveloperPayload?
    ) {
        mBillingClient.consumeAsync(
            ConsumeParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .setDeveloperPayload(payload?.toJson())
                .build(),
            listener
        )
    }

    fun acknowledgePurchase(
        purchaseToken: String,
        listener: AcknowledgePurchaseResponseListener,
        payload: DeveloperPayload?
    ) {
        mBillingClient.acknowledgePurchase(
            AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .setDeveloperPayload(payload?.toJson())
                .build(),
            listener
        )
    }

    fun consumeAsyncByID(
        iapProductId: String,
        listener: ConsumeResponseListener,
        payload: DeveloperPayload
    ) {
        mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP) { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                purchases.forEach { p ->
                    if (p.sku == iapProductId) {
                        consumeAsync(p.purchaseToken, listener, payload)
                        return@forEach
                    }
                }
            }
        }
    }

    fun queryPurchases(skuType: String): Purchase.PurchasesResult {
        return mBillingClient.queryPurchases(skuType)
    }

    fun setOnBillingServiceStateListener(listener: OnBillingServiceStateListener) {
        mBillingServiceStateListener = listener
    }

    /**
     * PUL-232: Show Discount Subscription Package for first open
     */
    fun hasBought(iap: String, onResult: (hasBought: Boolean) -> Unit) {
        mBillingClient.queryPurchaseHistoryAsync(
            BillingClient.SkuType.SUBS
        ) { billingResult, purchaseHistoryRecordList ->
            var found = false

            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                purchaseHistoryRecordList.forEach {
                    if (it.sku == iap) {
                        found = true
                    }
                }
            }

            onResult(found)
        }
    }

    /**
     * Return the list of purchase that is pending to verify with your server
     */
    fun loadPendingVerifyProduct(onQueryResult: (purchase: ArrayList<Purchase>) -> Unit) {
        val list = ArrayList<Purchase>()

        // IN-APP
        val inApp = queryPurchases(BillingClient.SkuType.INAPP)
        if (inApp.responseCode == BillingClient.BillingResponseCode.OK) {
            inApp.purchasesList.forEach { p ->
                Logger.d("AppBillingClient -> loadData INAPP: ${p.originalJson}, isAcknowledged: ${p.isAcknowledged}")
                if (!p.isAcknowledged) {
                    list.add(p)
                }
            }
        }

        // SUBS
        val sub = queryPurchases(BillingClient.SkuType.SUBS)
        if (sub.responseCode == BillingClient.BillingResponseCode.OK) {
            sub.purchasesList.forEach { p ->
                Logger.d("AppBillingClient -> loadData SUB: ${p.originalJson}, isAcknowledged: ${p.isAcknowledged}")
                if (!p.isAcknowledged) {
                    list.add(p)
                }
            }
        }

        onQueryResult(list)
    }

    // Create a singleton
    // https://github.com/googlesamples/android-architecture-components/blob/master/BasicRxJavaSampleKotlin/app/src/main/java/com/example/android/observability/persistence/UsersDatabase.kt
    companion object {
        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: AppBillingClient? = null

        fun getInstance(context: Context): AppBillingClient =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: AppBillingClient(context.applicationContext).also { INSTANCE = it }
            }

        fun getResponseCodeAsString(responseCode: Int): String {
            return when (responseCode) {
                BillingClient.BillingResponseCode.SERVICE_TIMEOUT -> "SERVICE_TIMEOUT"
                BillingClient.BillingResponseCode.USER_CANCELED -> "USER_CANCELED"
                BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED -> "FEATURE_NOT_SUPPORTED"
                BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> "SERVICE_DISCONNECTED"
                BillingClient.BillingResponseCode.OK -> "OK"
                BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE -> "SERVICE_UNAVAILABLE"
                BillingClient.BillingResponseCode.BILLING_UNAVAILABLE -> "BILLING_UNAVAILABLE"
                BillingClient.BillingResponseCode.ITEM_UNAVAILABLE -> "ITEM_UNAVAILABLE"
                BillingClient.BillingResponseCode.DEVELOPER_ERROR -> "DEVELOPER_ERROR"
                BillingClient.BillingResponseCode.ERROR -> "ERROR"
                BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> "ITEM_ALREADY_OWNED"
                else -> "ITEM_NOT_OWNED"
            }
        }
    }

    interface OnBillingServiceStateListener {
        fun onBillingServiceConnected()
        fun onBillingServiceDisconnected()
    }

    abstract class DeveloperPayload {
        abstract fun toJson(): String
    }

    abstract class ProductModel {
        abstract val skuDetails: SkuDetails
    }
}