package global.weventure.payment.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsResponseListener
import global.weventure.payment.model.AppBillingClient
import global.weventure.payment.model.BroadcastUtil
import global.weventure.payment.model.BroadcastUtil.INTENT_BILLING_CLIENT_CONNECTED
import global.weventure.payment.model.BroadcastUtil.RECEIVER_BILLING_CLIENT_STATE_CHANGE
import global.weventure.payment.model.Logger

abstract class PaymentActivity : AppCompatActivity() {
    private lateinit var mAppBillingClient: AppBillingClient

    private var mIsBillingClientConnected: Boolean = false
    val isBillingClientConnected: Boolean
        get() = mIsBillingClientConnected

    private var mIsBuyingProduct = false
    val isBuyingProduct: Boolean
        get() {
            return mIsBuyingProduct
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentView())

        registerBillingClientStateChangeReceiver()

        mAppBillingClient = AppBillingClient.getInstance(applicationContext)
    }

    override fun onDestroy() {
        super.onDestroy()

        unregisterBillingClientStateChangeReceiver()
    }

    abstract fun getContentView(): Int

    /**
     * [onError]: the error code is in [com.android.billingclient.api.BillingClient.BillingResponseCode]
     */
    protected fun buyProductOnGoogle(
        product: AppBillingClient.ProductModel,
        onSuccess: (purchase: List<Purchase>) -> Unit,
        onError: (errorCode: Int) -> Unit
    ) {
        mIsBuyingProduct = true

        mAppBillingClient.buyProduct(
            this,
            product,
            { purchases ->
                onSuccess(purchases)
            },
            { errorCode ->
                mIsBuyingProduct = false
                onError(errorCode)
            }
        )
    }

    protected fun findSkuDetail(
        sku: String,
        @BillingClient.SkuType skuType: String,
        onResult: (SkuDetails?) -> Unit
    ) {
        findSkuDetailList(arrayListOf(sku), skuType) { skuDetailsList ->
            if (skuDetailsList.isNotEmpty()) {
                // Pick the first one
                onResult(skuDetailsList[0])
            } else {
                onResult(null)
            }
        }
    }

    protected fun findSkuDetailList(
        skuList: ArrayList<String>,
        @BillingClient.SkuType skuType: String,
        onResult: (List<SkuDetails>) -> Unit
    ) {
        if (skuList.isEmpty()) {
            onResult(emptyList())
            return
        }

        // Query SkuDetail
        mAppBillingClient.querySkuDetailsAsync(
            skuList,
            skuType,
            SkuDetailsResponseListener { responseCode, skuDetailsList ->
                if (responseCode.responseCode == BillingClient.BillingResponseCode.OK) {
                    if (skuDetailsList.isNotEmpty()) {
                        onResult(skuDetailsList)
                        return@SkuDetailsResponseListener
                    }
                }

                // Not found
                onResult(emptyList())
            }
        )
    }

    protected fun hasBought(iap: String, onResult: (hasBought: Boolean) -> Unit) {
        mAppBillingClient.hasBought(iap, onResult)
    }

    /**
     * Return the list of purchase that is pending to verify with your server
     */
    protected fun loadPendingVerifyProduct(onQueryResult: (purchase: ArrayList<Purchase>) -> Unit) {
        mAppBillingClient.loadPendingVerifyProduct(onQueryResult)
    }

    protected fun parseErrorCodeToString(errorCode: Int): String {
        return AppBillingClient.getResponseCodeAsString(errorCode)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Broadcast receiver
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun registerBillingClientStateChangeReceiver() {
        registerReceiver(
            mBillingClientStateChangeReceiver,
            IntentFilter(
                BroadcastUtil.formatReceiverAction(this, RECEIVER_BILLING_CLIENT_STATE_CHANGE)
            )
        )
    }

    private fun unregisterBillingClientStateChangeReceiver() {
        try {
            unregisterReceiver(mBillingClientStateChangeReceiver)
        } catch (ex: java.lang.Exception) {
        }
    }

    private val mBillingClientStateChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (isFinishing || intent == null)
                return

            val isConnected = intent.getBooleanExtra(INTENT_BILLING_CLIENT_CONNECTED, false)

            mIsBillingClientConnected = isConnected
            onBillingClientStateChange(isConnected)
        }
    }

    protected open fun onBillingClientStateChange(isConnected: Boolean) {
        Logger.d("PaymentActivity -> onBillingClientStateChange: $isConnected")
    }
}